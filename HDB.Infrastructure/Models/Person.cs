﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace HDB.Infrastructure.Models
{
    public partial class Person
    {
        public int Id { get; set; }
        public string PIN { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Mobile { get; set; }
        public DateTime RegistrationDate { get; set; }
        public DateTime? CancellationDate { get; set; }
        public int ProfessionId { get; set; }

        public Profession Profession { get; set; }

        public void Delete()
        {
            CancellationDate = CancellationDate ?? DateTime.Now;
        }

        public void Update(Person person)
        {
            PIN = person.PIN;
            Firstname = person.Firstname;
            Lastname = person.Lastname;
            DateOfBirth = person.DateOfBirth;
            Mobile = person.Mobile;
            ProfessionId = person.ProfessionId;
        }

        public bool IsDuplicate(Person person)
        {
            return person.PIN == PIN && Age > 30;
        }

        [NotMapped]
        public int Age
        {
            get
            {
                var age = DateTime.Today.Year - DateOfBirth.Year;
                return DateOfBirth > DateTime.Today.AddYears(-age) ? --age : age;
            }
        }
    }
}