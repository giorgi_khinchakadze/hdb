﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HDB.Web.Models.Data;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace HDB.Web.Services.Interfaces
{
    public interface IHomeService
    {
        IEnumerable<SelectListItem> GetProfessionsSelectList();
        bool CreatePerson(Person person);
        IEnumerable<Person> GetPeople();
        void DeletePerson(int id);
        bool UpdatePerson(Person person);
        Person GetById(int id);
        void DeleteDuplicates();
    }
}
