﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HDB.Web.Models
{
    public class JsonResponse
    {
        public bool Error { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
    }
}