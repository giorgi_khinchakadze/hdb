﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HDB.Infrastructure.Models;
using HDB.Infrastructure.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace HDB.Infrastructure.Repositories
{
    public class PeopleRepository : BaseRepository, IPeopleRepository
    {
        public PeopleRepository(HDBContext context) : base(context)
        {
        }

        public List<Profession> GetProfessions()
        {
            return ctx.Professions.ToList();
        }

        public void Save(Person person)
        {
            ctx.People.Add(person);
            ctx.SaveChanges();
        }

        public List<Person> GetPeople()
        {
            return ctx.People.Include(p => p.Profession).ToList();
        }

        public void Delete(int id)
        {
            var person = ctx.People.First(p => p.Id == id);
            person.Delete();
            ctx.SaveChanges();
        }

        public void Update(Person person)
        {
            var entity = ctx.People.First(p => p.Id == person.Id);
            entity.Update(person);
            ctx.SaveChanges();
        }

        public Person GetById(int id)
        {
            return ctx.People.First(p => p.Id == id);
        }


        public void DeleteDuplicates()
        {
            ctx.People.FromSql("SELECT * FROM duplicate_people").ToList().ForEach(p => p.Delete());
            ctx.SaveChanges();
        }
    }
}