﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;

namespace HDB.Web.Services
{
    public class BaseService
    {
        protected IMapper Mapper { get; }

        public BaseService(IMapper mapper)
        {
            Mapper = mapper;
        }
    }
}