﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HDB.Web.Models.Data
{
    public class Person
    {
        public Person()
        {
            DateOfBirth = DateTime.Now;
            RegistrationDate = DateTime.Now;
        }

        public int Id { get; set; }

        [StringLength(11, MinimumLength = 11, ErrorMessage = "Length must be 11")]
        [RegularExpression("^[0-9]+$", ErrorMessage = "Must be numbers")]
        [Required]
        public string PIN { get; set; }

        [Required]
        public string Firstname { get; set; }

        [Required]
        public string Lastname { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Date of Birth")]
        public DateTime DateOfBirth { get; set; }


        [RegularExpression("^[+]?[0-9 ]+$", ErrorMessage = "Invalid format")]
        [Required]
        [DataType(DataType.PhoneNumber)]
        public string Mobile { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Registration Date")]
        public DateTime RegistrationDate { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Cancellation Date")]
        public DateTime? CancellationDate { get; set; }

        public Profession Profession { get; set; }

        [Required]
        [Display(Name = "Profession")]
        public int ProfessionId { get; set; }

        public int Age { get; set; }
    }
}