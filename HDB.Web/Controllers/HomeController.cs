﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HDB.Web.Models;
using HDB.Web.Models.Data;
using HDB.Web.Services.Interfaces;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace HDB.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IHomeService _homeService;

        public HomeController(IHomeService homeService)
        {
            _homeService = homeService;
        }

        public IActionResult Index()
        {
            var data = _homeService.GetPeople();
            return View(data);
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }

        public IActionResult Create()
        {
            return View(new Person());
        }

        [HttpPost]
        public IActionResult Create(Person person)
        {
            if (ModelState.IsValid)
            {
                if (_homeService.CreatePerson(person))
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("Summary", "General Error");
                }
            }

            return View(person);
        }


        [HttpDelete]
        public void Delete(int id)
        {
            _homeService.DeletePerson(id);
        }

        public JsonResult People()
        {
            var data = _homeService.GetPeople();
            var respnse = new JsonResponse()
            {
                Data = data,
                Error = false
            };
            return Json(respnse);
        }


        public IActionResult Edit(int id)
        {
            var data = _homeService.GetById(id);
            return PartialView("Partials/_EditModal", data);
        }


        [HttpPost]
        public JsonResult Edit(Person person)
        {
            var response = new JsonResponse();
            ;
            if (ModelState.IsValid)
            {
                if (!_homeService.UpdatePerson(person))
                {
                    response.Error = true;
                }
            }
            else
            {
                response.Error = true;

                var errors = new Dictionary<string, string[]>();
                foreach (var key in ModelState.Keys.Where(s =>
                    ModelState[s].ValidationState == ModelValidationState.Invalid))
                {
                    errors.Add(key, ModelState[key].Errors.Select(e => e.ErrorMessage).ToArray());
                }

                response.Data = errors;
            }

            return Json(response);
        }

        public void DeleteDuplicates()
        {
            _homeService.DeleteDuplicates();
        }
    }
}