﻿using System;
using System.Windows.Forms;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace HDB.Infrastructure.Models
{
    public class HDBContext : DbContext
    {
        private readonly string _connectionString;


        public HDBContext(DbContextOptions options) : base(options)
        {
        }

        public HDBContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!string.IsNullOrWhiteSpace(_connectionString))
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }


        public virtual DbSet<Person> People { get; set; }
        public virtual DbSet<Profession> Professions { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Person>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CancellationDate)
                    .HasColumnName("cancellation_date")
                    .HasColumnType("date");

                entity.Property(e => e.DateOfBirth)
                    .HasColumnName("date_of_birth")
                    .HasColumnType("date");

                entity.Property(e => e.Firstname)
                    .IsRequired()
                    .HasColumnName("firstname")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Lastname)
                    .IsRequired()
                    .HasColumnName("lastname")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Mobile)
                    .IsRequired()
                    .HasColumnName("mobile")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PIN)
                    .IsRequired()
                    .HasColumnName("pin")
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.ProfessionId).HasColumnName("profession_id");

                entity.Property(e => e.RegistrationDate)
                    .HasColumnName("registration_date")
                    .HasColumnType("date")
                    .HasDefaultValueSql("(sysdatetime())");

                entity.HasOne(d => d.Profession)
                    .WithMany(p => p.People)
                    .HasForeignKey(d => d.ProfessionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("people_professions_fk");
            });

            modelBuilder.Entity<Profession>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });
        }
    }
}