﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using HDB.Infrastructure.Repositories.Interfaces;
using HDB.Web.Models.Data;
using HDB.Web.Services.Interfaces;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace HDB.Web.Services
{
    public class HomeService : BaseService, IHomeService
    {
        private readonly IPeopleRepository _peopleRepository;

        public HomeService(IMapper mapper, IPeopleRepository peopleRepository) : base(mapper)
        {
            _peopleRepository = peopleRepository;
        }

        public IEnumerable<SelectListItem> GetProfessionsSelectList()
        {
            var professions = _peopleRepository.GetProfessions();
            return professions.Select(p => new SelectListItem()
            {
                Value = $"{p.Id}",
                Text = p.Name
            }).ToList();
        }

        public bool CreatePerson(Person person)
        {
            try
            {
                _peopleRepository.Save(Mapper.Map<Infrastructure.Models.Person>(person));
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public IEnumerable<Person> GetPeople()
        {
            return Mapper.Map<List<Person>>(_peopleRepository.GetPeople());
        }


        //TODO 
        public void DeletePerson(int id)
        {
            _peopleRepository.Delete(id);
        }

        public bool UpdatePerson(Person person)
        {
            try
            {
                _peopleRepository.Update(Mapper.Map<Infrastructure.Models.Person>(person));
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public Person GetById(int id)
        {
            return Mapper.Map<Person>(_peopleRepository.GetById(id));
        }

        public void DeleteDuplicates()
        {
            _peopleRepository.DeleteDuplicates();
        }
    }
}