﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using FluentScheduler;
using HDB.Infrastructure.Models;
using HDB.Infrastructure.Repositories;
using HDB.Infrastructure.Repositories.Interfaces;
using Topshelf;

namespace HDB.Service
{
    class DuplicateRemovalService : ServiceControl
    {
        private readonly string _connectionString = ConfigurationManager.ConnectionStrings["HDB"].ConnectionString;

        public DuplicateRemovalService()
        {
        }

        public bool Start(HostControl hostControl)
        {
            JobManager.AddJob(() => new PeopleRepository(new HDBContext(_connectionString)).DeleteDuplicates(),
                s => s.NonReentrant().ToRunNow().AndEvery(10).Seconds());

            return true;
        }

        public bool Stop(HostControl hostControl)
        {
            return true;
        }
    }
}