﻿// Write your JavaScript code.


function tableInit() {
    $('#people-table').find('[data-toggle=confirmation]').confirmation({
        rootSelector: '[data-toggle=confirmation]',
    });


    $('#people-table').find('[data-action=delete]').on('confirmed.bs.confirmation',
        function() {
            var data = {};
            data.id = $(this).data('id');

            var url = '/Home/Delete';
            $.ajax({
                url: url,
                method: 'DELETE',
                data: data
            }).then(() => {
                $('#people-table').DataTable().ajax.reload();
            });
        });

    $('#people-table').find('[data-action=edit]').click(function() {
        var data = {};
        data.id = $(this).data('id');

        var url = '/Home/Edit';
        $.ajax({
            url: url,
            method: 'GET',
            data: data
        }).then((data) => {
            $('#modal .modal-content').html(data);
            $('#modal').modal('show');
        });
    });
}

$(() => {
    $('#people-table').dataTable({
        ajax: {
            url: '/Home/People',
        },
        columns: [
            { data: "pin" },
            { data: "firstname" },
            { data: "lastname" },
            { data: "age" },
            { data: "mobile" },
            { data: "profession.name" },
            { data: "registrationDate" },
            { data: "cancellationDate" },
            {
                data: "id",
                render: function(data, type, row) {
                    var template = `<a  data-action="edit"
                                                data-id="${data}"
                                                class="btn btn-xs btn-primary">
                                                <span class="fa fa-edit"></span>
                                            </a>`;

                    if (!row.cancellationDate) {
                        template += `<a data-action="delete"
                                                data-id="${data}"
                                                data-toggle="confirmation"
                                                class="btn btn-xs btn-danger"
                                                style="margin-left: 10px">
                                                <span class="fa fa-trash"></span>
                                             </a>`;
                    }
                    return template;
                }
            }
        ],
        drawCallback: function() {
            tableInit();
        }
    });

});

$(() => {
    $('#modal').on('click',
        '#save-button',
        function() {
            var form = $('#person-edit-form');
            var data = form.serializeArray();
            var url = '/home/edit';
            $.ajax({
                method: 'POST',
                data: data,
                url: url
            }).then(function(response) {
                if (response.error) {
                    form.fillFormErrors(response.data);
                } else {
                    $('#modal').modal('hide');
                    $('#people-table').DataTable().ajax.reload();
                }
            });
        });
});

$.fn.fillFormErrors = function(data) {
    $(this).find('[data-valmsg-fo]').html('');
    $.each(data,
        (k, v) => {
            var span = $(this).find(`[data-valmsg-for="${k}"]`);
            span.text(v);
        });
}


$(() => {
    $('#delete-duplicates').click(function() {

        $.ajax({
            url: '/home/deleteduplicates',
            method: 'GET',
        }).then(() => {
            $('#people-table').DataTable().ajax.reload();
        });
    });
})