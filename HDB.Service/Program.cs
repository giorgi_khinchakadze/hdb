﻿using System;
using System.Configuration;
using HDB.Infrastructure.Models;
using HDB.Infrastructure.Repositories;
using HDB.Infrastructure.Repositories.Interfaces;
using Topshelf;
using Topshelf.ServiceConfigurators;

namespace HDB.Service
{
    class Program
    {
        static void Main(string[] args)
        {
            HostFactory.Run(x =>
            {
                x.Service(delegate(ServiceConfigurator<DuplicateRemovalService> configurator)
                {
                    configurator.ConstructUsing(() => new DuplicateRemovalService());
                    configurator.WhenStarted((service, control) => service.Start(control));
                    configurator.WhenStopped((service, control) => service.Stop(control));
                });

                x.SetDescription("Removes Duplicates");
                x.SetDisplayName("HDB.Service");
                x.SetServiceName("HDB.Service");
            });

            Console.ReadKey();
        }
    }
}