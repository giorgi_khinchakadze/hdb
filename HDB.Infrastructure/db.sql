﻿CREATE DATABASE HDB;
GO

USE HDB;

GO

CREATE TABLE Professions (
  id   INT IDENTITY PRIMARY KEY,
  name VARCHAR(100) NOT NULL,
);


CREATE TABLE People (
  id                INT                   IDENTITY PRIMARY KEY,
  pin               VARCHAR(11)  NOT NULL,
  firstname         VARCHAR(100) NOT NULL,
  lastname          VARCHAR(100) NOT NULL,
  date_of_birth     DATE         NOT NULL,
  mobile            VARCHAR(100) NOT NULL,
  registration_date DATE         NOT NULL DEFAULT SYSDATETIME(),
  cancellation_date DATE,
  profession_id     INT          NOT NULL,

  CONSTRAINT people_professions_fk FOREIGN KEY (profession_id) REFERENCES professions (id)
);

INSERT INTO Professions (name) VALUES ('p1'), ('p2');


CREATE VIEW duplicate_people AS
  SELECT *
  FROM people p
  WHERE p.pin IN (SELECT p1.pin
                  FROM people p1
                  WHERE p1.cancellation_date IS NULL
                  GROUP BY p1.pin
                  HAVING count(1) > 1) AND

        (datediff(YY, p.date_of_birth, GETDATE()) -
         CASE WHEN dateadd(YY, DATEDIFF(YY, p.date_of_birth, getdate()), p.date_of_birth) > GETDATE()
           THEN 1
         ELSE 0 END) > 30
        AND p.cancellation_date IS NULL

