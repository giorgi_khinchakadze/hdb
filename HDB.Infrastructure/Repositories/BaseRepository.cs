﻿using System;
using System.Collections.Generic;
using System.Text;
using HDB.Infrastructure.Models;

namespace HDB.Infrastructure.Repositories
{
    public class BaseRepository : IDisposable
    {
        protected readonly HDBContext ctx;

        public BaseRepository(HDBContext context)
        {
            ctx = context;
        }

        public void Dispose()
        {
            ctx?.Dispose();
        }
    }
}