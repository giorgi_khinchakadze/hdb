﻿using System;
using System.Collections.Generic;

namespace HDB.Infrastructure.Models
{
    public partial class Profession
    {
        public Profession()
        {
            People = new HashSet<Person>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<Person> People { get; set; }
    }
}
