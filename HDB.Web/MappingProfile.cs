﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using HDB.Infrastructure.Models;

namespace HDB.Web
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Person, Models.Data.Person>().ReverseMap();
            CreateMap<Profession, Models.Data.Profession>().ReverseMap();

        }
    }
}