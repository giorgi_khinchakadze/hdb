﻿using System;
using System.Collections.Generic;
using System.Text;
using HDB.Infrastructure.Models;

namespace HDB.Infrastructure.Repositories.Interfaces
{
    public interface IPeopleRepository
    {
        List<Profession> GetProfessions();
        void Save(Person person);
        List<Person> GetPeople();
        void Delete(int id);
        void Update(Person person);
        Person GetById(int id);
        void DeleteDuplicates();
    }
}